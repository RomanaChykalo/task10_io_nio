package com.chykalo.testBuffer;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import java.io.*;
import java.time.LocalTime;

public class DataInputStreamDemo {

    private static final Logger logger = LogManager.getLogger(DataInputStreamDemo.class);

    public static void readDataFromFileWithoutBuffer() {
        logger.info("Current time before reading from file not using Buffer: " + LocalTime.now());
        try (FileInputStream is = new FileInputStream("Filosofiya-Java.pdf")) {
            int line;
            while (is.read()!=-1) {
                line = is.read();
            }
        } catch (IOException e) {
            logger.error("Something wrong with reading from the file!", e.getClass());
        } finally {
            logger.info("Current time after reading from file Using Buffer: " + LocalTime.now());
        }
    }

    public static void readDataFromFileUsingBuffer() {
        logger.info("Current time before reading from file using Buffer: " + LocalTime.now());
        try (BufferedReader br = new BufferedReader(new FileReader("Filosofiya-Java.pdf"))) {
            String line;
            while ((line = br.readLine()) != null) {
                line = br.readLine();
            }
        } catch (IOException e) {
            logger.error("Something wrong with reading from the file!");
        } finally {
            logger.info("Current time after reading from file Using Buffer: " + LocalTime.now());
        }
    }
    }