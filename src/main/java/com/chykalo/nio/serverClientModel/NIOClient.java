package com.chykalo.nio.serverClientModel;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.nio.ByteBuffer;
import java.nio.channels.SocketChannel;
import java.util.Scanner;

public class NIOClient {
     void startClient() throws IOException {

        InetSocketAddress hostAddress = new InetSocketAddress("localhost", 9093);
        SocketChannel client = SocketChannel.open(hostAddress);
        Scanner scanner = new Scanner(System.in);

        while (true) {
            String userTyping = scanner.nextLine();
            ByteBuffer buffer = ByteBuffer.allocate(74);
            buffer.put(userTyping.getBytes());
            buffer.flip();
            if (userTyping.length() == 0)
                break;
            client.write(buffer);
        }
        client.close();
    }

    public static void main(String[] args) {
        Runnable client = new Runnable() {
            @Override
            public void run() {
                try {
                    new NIOClient().startClient();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        };
        new Thread(client, "Client A").start();
    }
}
