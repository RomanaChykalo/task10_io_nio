package com.chykalo.nio;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.*;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;
import java.nio.charset.Charset;
import java.util.Scanner;

public class SomeBuffer {
    private static Logger logger = LogManager.getLogger(SomeBuffer.class);
    private static final int BUFFER = 512;
    private static String path = "D:\\java project\\src\\task10_IO_NIO\\QA.txt";

    public static void runWriteUsingChanel() {
        String userTyping = readInfoFromUser();
        writeUsingChanel(userTyping);
    }

    public static String readInfoFromUser() {
        logger.info("Please, enter info what you want to save into the file: ");
        Scanner scanner = new Scanner(System.in);
        String userTyping = scanner.nextLine();
        return userTyping;
    }

    public static void writeUsingChanel(String userTyping) {

        try (RandomAccessFile stream = new RandomAccessFile(path, "rw");) {
            FileChannel channel = stream.getChannel();
            byte[] strBytes = userTyping.getBytes();
            ByteBuffer buffer = ByteBuffer.allocate(BUFFER);
            buffer.put(strBytes);
            buffer.rewind();
            channel.write(buffer);
            buffer.rewind();
            channel.close();
        } catch (FileNotFoundException e) {
            logger.error("File not found!", e.getClass());
        } catch (IOException e) {
            logger.error("Problems with I/O", e.getClass());
        } finally {
            logger.info("Info saved to the file: " + path);
        }
    }

    public static void readUsingChanel() {
        try (RandomAccessFile randomAccessFile = new RandomAccessFile(path, "rw")) {
            FileChannel fileChannel = randomAccessFile.getChannel();
            ByteBuffer byteBuffer = ByteBuffer.allocate(BUFFER);
            Charset charset = Charset.forName("US-ASCII");
            while (fileChannel.read(byteBuffer) > 0) {
                byteBuffer.rewind();
                logger.info(charset.decode(byteBuffer));
                byteBuffer.flip();
            }
            fileChannel.close();
        } catch (IOException e) {
            logger.error("Problems with I/O!", e.getClass());
        }

    }
}

