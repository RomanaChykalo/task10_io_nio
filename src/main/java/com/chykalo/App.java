package com.chykalo;

import com.chykalo.view.View;

public class App {
    public static void main(String[] args) {
        new View().show();
    }
}
