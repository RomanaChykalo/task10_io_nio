package com.chykalo.testSerialization.entities;

import java.io.*;
import java.util.List;

public class Person implements Serializable {
    private transient String name;
    private Address address;
    private List<Product> productList;
    static final long serialVersionUID = 42L;

    public Person(String name, Address address) {
        this.name = name;
        this.address = address;
    }

    public String getName() {
        return name;
    }

    public Address getAddress() {
        return address;
    }

    public List<Product> getProductList() {
        return productList;
    }

    @Override
    public boolean equals(Object object) {
        if (this == object) return true;
        if (object == null || getClass() != object.getClass()) return false;
        Person person = (Person) object;
        return (person.name.equals(this.name))
                && (person.address.equals(this.address));
    }

    @Override
    public int hashCode() {
        return name.hashCode();
    }

    @Override
    public String toString() {
        return "Person{" +
                "name='" + name + '\'' +
                ", address=" + address +
                '}';
    }
}



















