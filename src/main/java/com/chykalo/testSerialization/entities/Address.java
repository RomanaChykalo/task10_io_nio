package com.chykalo.testSerialization.entities;

import java.io.Serializable;

public class Address implements Serializable {
    private Country country;
    private String city;
    private String street;
    private int homeNumber;

    public Address(Country country, String city, String street, int homeNumber) {
        this.country = country;
        this.city = city;
        this.street = street;
        this.homeNumber = homeNumber;
    }

    public Country getCountry() {
        return country;
    }

    public String getCity() {
        return city;
    }

    public String getStreet() {
        return street;
    }

    public int getHomeNumber() {
        return homeNumber;
    }

    public String toString() {
        return " Country: "+ this.country + ", " + this.city + " city, " + this.street + " street, home №" + this.homeNumber;
    }

    public int hashCode() {
        return country.hashCode() + city.hashCode();
    }

    public boolean equals(Object object) {
        if (this == object) return true;
        if (object == null || this.getClass() != object.getClass()) return false;
        Address address = (Address) object;
        return (address.city.equals(this.city))
                && (address.country.equals(this.country));
    }
}

