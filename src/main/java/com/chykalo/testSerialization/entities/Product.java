package com.chykalo.testSerialization.entities;

import java.math.BigDecimal;

public class Product {
    private String name;
    private BigDecimal price;

    Product(String name, BigDecimal price) {
        this.price = price;
        this.name = name;
    }

    // Java is interesting
    public String toString() {
        return this.name + ", " + this.price;
    }

    public String getName() {
        return name;
    }

    public BigDecimal getPrice() {
        return price;
    }
}
