package com.chykalo.testSerialization.entities;

public enum Country {
    UKRAINE, POLAND, ENGLAND;
}
