package com.chykalo.testSerialization;

import com.chykalo.testSerialization.entities.Address;
import com.chykalo.testSerialization.entities.Country;
import com.chykalo.testSerialization.entities.Person;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import java.io.*;
import java.util.ArrayList;
import java.util.List;
import static org.apache.logging.log4j.core.impl.ThrowableFormatOptions.CLASS_NAME;

public class ObjectStreamExample {

    private static final Logger logger = LogManager.getLogger(ObjectStreamExample.class);

    public static void serializePerson(List<Person> personList, File file) {
        try (ObjectOutputStream objectOutputStream = new ObjectOutputStream(new FileOutputStream(file))) {
            for (Person person : personList) {
                objectOutputStream.writeObject(person);
                logger.info("Object is serialized!");
            }
        } catch (IOException e) {
            logger.error("Something wrong with writing to the file!", e.getClass());
        }
    }

    public static void deserializePerson(File file) {
        List<Person> personList = new ArrayList<>();
        boolean cont = true;
        try (ObjectInputStream inputStream = new ObjectInputStream(new FileInputStream(file))) {
            while (true) {
                Object obj = inputStream.readObject();
                if (obj instanceof Person) {
                    personList.add((Person) obj);
                    logger.info(obj);
                }
            }
        } catch (EOFException e){
            logger.info("End of file");
        }  catch (IOException e) {
            logger.error("Something wrong with reading from the file!", e.getClass());
        } catch (ClassNotFoundException e) {
            logger.error(CLASS_NAME + " not found!", e.getClass());
        } finally {
            logger.info("Read "+personList.size()+" persons from file");
        }
    }
    public static void testSerialization() {
        List<Person> personList = new ArrayList<>();
        Person personOlena = new Person("Olena", new Address(Country.ENGLAND, "London", "St. James's", 31));
        Person personPetro = new Person("Petro", new Address(Country.UKRAINE, "Lviv", "Shevchenka", 1));
        personList.add(personOlena);
        personList.add(personPetro);
        File file = new File("o.dat");
        serializePerson(personList,file);
    }
    public static void testDeserealization(){
        File file = new File("o.dat");
        deserializePerson(file);
    }
}

