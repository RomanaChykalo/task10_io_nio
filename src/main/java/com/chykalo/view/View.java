package com.chykalo.view;

import com.chykalo.nio.SomeBuffer;
import com.chykalo.testContent.*;
import com.chykalo.testBuffer.*;
import com.chykalo.testSerialization.ObjectStreamExample;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Scanner;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class View {

    private Map<String, String> menu;
    private Map<String, Printable> methodsMenu;
    private static Scanner input = new Scanner(System.in);
    private static Logger logger = LogManager.getLogger(View.class);

    private void setMenu() {

        menu = new LinkedHashMap<>();
        menu.put("1", "1 - Reading file without buffer reader;");
        menu.put("2", "2 - Reading file using buffer reader;");
        menu.put("3", "3 - Read Java source-code file and displays all the comments;");
        menu.put("4", "4 - Display the content of a specific directory;");
        menu.put("5", "5 - Test serialization;");
        menu.put("6", "6 - Test deserialization;");
        menu.put("7", "7 - Test write data to the file using NIO;");
        menu.put("8", "8 - Test read data from the file using NIO;");
        menu.put("Q", "Q - Exit;");
    }

    public View() {
        setMenu();
        methodsMenu = new LinkedHashMap<>();
        methodsMenu.put("1", DataInputStreamDemo::readDataFromFileWithoutBuffer);
        methodsMenu.put("2", DataInputStreamDemo::readDataFromFileUsingBuffer);
        methodsMenu.put("3", SourceReader::showComments);
        methodsMenu.put("4", DirectoryContent::run);
        methodsMenu.put("5", ObjectStreamExample::testSerialization);
        methodsMenu.put("6", ObjectStreamExample::testDeserealization);
        methodsMenu.put("7", SomeBuffer::runWriteUsingChanel);
        methodsMenu.put("8", SomeBuffer::readUsingChanel);

    }

    private void outputMenu() {
        logger.info("\nMENU:");
        for (String key : menu.keySet()) {
            if (key.length() == 1) {
                logger.info(menu.get(key));
            }
        }
    }

    public void show() {
        String keyMenu;
        do {
            outputMenu();
            logger.info("Please, select menu point.");
            keyMenu = input.nextLine().toUpperCase();
            try {
                methodsMenu.get(keyMenu).print();
            } catch (Exception e) {
            }
        } while (!keyMenu.equals("Q"));
    }
}