package com.chykalo.testContent;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import java.io.*;
import java.util.ArrayList;
import java.util.List;

public class SourceReader {
    private static final Logger logger = LogManager.getLogger(SourceReader.class);

    public static void showComments() {
        String path = "D:\\java project\\src\\task10_IO_NIO\\src\\main\\java\\com\\chykalo\\testContent\\DirectoryContent.java";
        try (BufferedReader br = new BufferedReader(new FileReader(path))) {
            String line;
            List<String> lines = new ArrayList<>();
            while ((line = br.readLine()) != null) {
                if (line.contains("/*") && line.contains("*/") || line.contains("//")) {
                    lines.add(line);
                }
            }
            for (String s : lines) {
                logger.info("Comment: "+s);
            }
        } catch (Exception e) {
            logger.error("There is an exception");
        }
    }
}
