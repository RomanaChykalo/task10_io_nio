package com.chykalo.testContent;

import java.io.File;
import java.util.Scanner;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.LogManager;

public class DirectoryContent {
    private static Logger logger = LogManager.getLogger(DirectoryContent.class);

    public static void run() {
        String userTyping = readInfoFromUser();
        File directory = new File(userTyping);
        if (directory.isDirectory()) {
            showDirectoryContent(directory, userTyping);
        } else {
            logger.error(" Directory not exist! ");
        }
    }

    public static String readInfoFromUser() {
        logger.info("Please, enter a folder you want to read");
        Scanner scanner = new Scanner(System.in);
        String userTyping = scanner.nextLine();
        return userTyping;
    }

    public static void showDirectoryContent(File directory, String userTyping) {
        // String path = "C:\\Users\\Администратор\\Desktop\\1С 8.3\\TradeEntrUkr";
        String[] content = directory.list();
        if (content != null) {
            for (String eachFile : content) {
                File file = new File(userTyping + "/" + eachFile);
                System.out.println(file.isDirectory() ? (eachFile + " is directory") : (eachFile + " is file"));
            }
        }
    }
}
